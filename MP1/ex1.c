#include <stdio.h> 
#include <string.h>
#include <stdlib.h>
#include <unistd.h> 
#include <sys/stat.h>
#include <unistd.h> 
#include <dirent.h>
#include <fcntl.h> 
#include <sys/types.h>

  void ini_tableau(char * commande1, char * * commande2) //Initialise les deux tableaux de commandes en entrée
  {
    int i;
    for (i = 0; i < 99; i++) {
      commande2[i] = 0;
      commande1[i] = 0;
    }
  }

int compare(char const * p, char const * q) //Compare entre deux chaines de caractères et retourne 0 en cas où les chaines sont égales
  {

    if ( * p != * q)
      return 1;
    return 0;
  }
int compare2(const char * chaine1, const char * chaine2) //Compare l'egalité de deux chaine de caracteres
  {

    if (chaine1 != NULL || chaine2 == NULL) {
      if ( * chaine1 != * chaine2) {
        return 0;
      } else
        return 1;
    } else
      return 0;
  }

void tokenize(char * * cmd) //Découpe la commande en évitant les espaces et les tabulations 
  {
    int nbtok = 1;
    char * pointer;
    pointer = strtok(cmd[0], " \t");
    cmd[1] = pointer;
    while (pointer != NULL) {
      pointer = strtok(NULL, " \t ");
      if (pointer != NULL) {
        nbtok = nbtok + 1;
        cmd[nbtok] = pointer;
      }

    }

  }
int main() {
  char * getcwd(char * buf, size_t size); // Reconnaissance du répertoire actuel
  char commande1[100];
  char cwd[1024];
  
  
  char * commande2[100];
  char * c1;


  while (1) {
    ini_tableau(commande1, commande2); //On commence par initialiser les tableaux commandes et nn à 0, sachant que commande prendra toute la commande au début et nn le décomposera entre ses cases
    do {
      if (getcwd(cwd, sizeof(cwd)) != NULL) //avoir l'emplacement du répertoire courant
        printf("%%");
      printf("%s", cwd); //affichage du répertoire courant
      c1 = fgets(commande1, 99, stdin); //
    } while (strlen(commande1) == 1 && c1 != NULL);

    commande1[strlen(commande1) - 1] = '\0';

    commande2[0] = commande1;
    commande2[10] = NULL;

    tokenize(commande2);

    if (!compare(commande2[1], "cd"))
      chdir(commande2[2]);

    if (!compare(commande2[1], "exit"))
      return (0);

    pid_t pid = fork();

    if (pid == 0) {

      execvp(commande2[1], & commande2[1]);

      exit(1);
    } else {
      wait(NULL);
    }

  }
  return (0);
}