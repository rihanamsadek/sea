#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>

int compare2(const char* chaine1, const char* chaine2)//Compare l'egalité de deux chaine de caracteres
{


 if(chaine1 != NULL || chaine2 == NULL)
 {
    if(*chaine1 != *chaine2)
    {
    return 0;
}
    else
    return 1;
}
else
return 0;
}

void ini_tableau(char *commande1,char **commande2) //Initialise les deux tableaux de commandes en entrée
{
  int i;
  for (i=0;i<99;i++)
  {
    commande2[i]=0;
    commande1[i]=0;
  }
}
int compare(char const *p, char const *q) //Compare entre deux chaines de caractères et retourne 0 en cas où les chaines sont égales
{
 if(p == NULL || q == NULL)
  return(1);
 if(*p != *q)
  return (1);
  return (0);
}
void tokenize (char** cmd ) //Découpe la commande en évitant les espaces et les tabulations 
{
  int nbtok=1;
  char* pointer;
  pointer=strtok(cmd[0]," \t");
  cmd[1]=pointer;
  while(pointer!= NULL)
  {
    pointer=strtok( NULL," \t ");
   if (pointer!= NULL)
   {
     nbtok=nbtok+1;
     cmd[nbtok]=pointer;
   }
    
  }  
  
}
int main(){
    char *getcwd(char *buf, size_t size); // Reconnaissance du répertoire actuel
    char commande1[100];
    char cwd[1024];
    char *commande2[100];
    char *c1;
    char *copie;
    int in, out,i;
    int p;
    while(1)
    { ini_tableau(commande1,commande2); //On commence par initialiser les tableaux commandes et nn à 0, sachant que commande prendra toute la commande au début et nn le décomposera entre ses cases
   do{
   if (getcwd(cwd, sizeof(cwd)) != NULL) //avoir l'emplacement du répertoire courant
    printf("%s",cwd);
    printf("%%"); //affichage du répertoire courant
    c1=fgets(commande1,99,stdin);//
   }while(strlen(commande1) ==1 && c1 != NULL);
    int second=0;
    commande1[strlen(commande1)-1]='\0';
    commande2[0]=commande1;
    
    tokenize(commande2); //Découpage de la commande entrée par l'utilisateur

        for(i=0;i< 10;i++)
        if (commande2[i]=='\0')
		{
			commande2[i]=NULL; //Nécessité d'avoir la dernière partie de la commande nulle.
		}
    char *cop;   
    for(i=0;i< 10;i++)

		{


		cop=NULL;
		cop=commande2[i];
			if(cop != NULL && i!= 0)
															
		    if(!strcmp(commande2[i],"&")) //Dans le cas où on retrouve le "&"
			{
				second=1; //On passe en tâche de fond après avoir retrouver le caractère "&"
				commande2[i]=NULL;
			}
		}

    if (!compare(commande2[1],"cd")) // Dans le cas de la commande'cd' on utilise la commande chdir pour changer de répertoire vers celui qui est saisi en deuxième après le cd
         chdir(commande2[2]); // La commande2[2] représente le répertoire/fichier vers lequel on se rédirige.
    if (!compare(commande2[1],"exit")) //Dans le de la saisie de la commande 'exit' il suffit de sortir de la boucle.
      return (0);

    pid_t pid=fork();  //Génération d'un processus pour exécuter les autres commandes. Sauf que là, on a des cas exceptionnels. On commencera par les redirections
    if(pid==0)         
   { 
    for(i=0;i<10;i++)  
    {  
    copie=NULL;
		copie=commande2[i];  
		if(copie != NULL)	 
    if(!strcmp(copie,">")) //1ère redirection : vers un fichier 
    {

    int file = open(commande2[i+1],O_TRUNC | O_WRONLY | O_CREAT);
    dup2(file,1);    
    close(file);
    commande2[i]=NULL;
     }
		
      
    if(copie != NULL)	 
    if(!strcmp(copie,">>"))  //2ème redirection : rajout d'une sortie vers un fichier déjà existant
    {

    int file = open(commande2[i+1],O_APPEND| O_WRONLY|O_CREAT);
    dup2(file,1);    
    close(file);
    commande2[i]=NULL;
}
		if(copie != NULL)	 
    if(!strcmp(copie,"<")) // 3ème redirection : rajout des données provenant d'un fichier
    {

    int file = open(commande2[i+1],O_RDONLY); // Les options changent, ici on n'a le droit que de lire, differemment aux autres redirections.
    dup2(file,0);    
    close(file);
    commande2[i]=NULL;
    }
    }

    for(i=0;i<10;i++)  
    { 
		commande2[i-1]=commande2[i];
	}
    execvp(commande2[0],commande2); //Exécution de toute autre commande.
    exit(1);
    

   }
   else
   if(second!=1)
     waitpid(pid,NULL,0); //Attente de la mort du processus fils par le père. 
   }
  return (0);
  }
