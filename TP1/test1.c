#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

int main()
{
    int pid;
    pid=fork();
    if(pid<0)
    {
        printf("\n Error ");
        exit(1);
    }
    else if(pid==0)
    {
        printf("\n Je suis le processus fils ");
        printf("\n Mon PID est %d ",getpid());
        wait(0);
    }
    else
    {
        printf("\n Je suis le processus père ");
        printf("\n Mon PID est %d \n ",getpid());
        exit(1);
    }

}
